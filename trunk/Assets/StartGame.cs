﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGame : MonoBehaviour {

    public Animator anim;

    public void START() {
        StartCoroutine(WaitForAnim());
    }

    public IEnumerator WaitForAnim() {
        anim.SetTrigger("Start");
        yield return new WaitForSeconds(2);
        GameFlow.WinLevel();
    }
}
