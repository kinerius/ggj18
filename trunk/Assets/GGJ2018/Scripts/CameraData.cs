﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "GGJ2018/CameraData", order = 1)]
public class CameraData : ScriptableObject {

    public float gameplaySideSpeed = 1;
    public float cameraLerpSpeed = 5;
    public float initialPosition = -10;

    public Transform UI;
}
