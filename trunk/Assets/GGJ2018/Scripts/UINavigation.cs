﻿
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
 
public class UINavigation : MonoBehaviour
{
    private EventSystem system;

    private void Start()
    {
        system = EventSystem.current;

    }

    private void Update()
    {
        Selectable next =  Input.GetButtonDown("HorizontalN") ? system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnLeft() : null;

        if (next)
        {
            system.SetSelectedGameObject(next.gameObject);
            return;
        }

        next =  Input.GetButtonDown("HorizontalP") ? system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnRight() : null;
        if (next)
        {
            system.SetSelectedGameObject(next.gameObject);
            return;
        }


    }
}
