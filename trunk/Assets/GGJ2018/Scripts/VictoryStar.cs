﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictoryStar : MonoBehaviour
{

    public bool catched;

    public void OnEnable()
    {
        GameFlow.RegisterStar(this);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        catched = true;
        GameFlow.StarCatch();
        gameObject.SetActive(false);
    }

}
