﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public enum UI_TYPE
{
    TUTORIAL,
    PAUSE,
    RESTART,
}

public class UIController : MonoBehaviour
{
    public Transform tutorial;
    public Transform pauseMenu;
    public Transform restartMenu;

    public Dictionary<UI_TYPE, Transform> _ui = new Dictionary<UI_TYPE, Transform>();

    private void Awake()
    {
        _ui.Add(UI_TYPE.TUTORIAL, tutorial);
        _ui.Add(UI_TYPE.PAUSE, pauseMenu);
        _ui.Add(UI_TYPE.RESTART, restartMenu);

        SetWindow(UI_TYPE.TUTORIAL, false);
        SetWindow(UI_TYPE.PAUSE, false);
        SetWindow(UI_TYPE.RESTART, false);
    }

    public void SetWindow(UI_TYPE type, bool enabled)
    {
        //if (type == UI_TYPE.PAUSE)
        //{
        //    Time.timeScale = enabled ? 0 : 1;
        //}

        _ui[type].gameObject.SetActive(enabled);

        Time.timeScale = (_ui[UI_TYPE.PAUSE].gameObject.activeInHierarchy
            || _ui[UI_TYPE.TUTORIAL].gameObject.activeInHierarchy) ? 0 : 1;

        if (enabled) {
            Button firstB = _ui[type].GetComponentInChildren<Button>(true);
            if (firstB && EventSystem.current.currentSelectedGameObject != firstB)
            {
                EventSystem.current.SetSelectedGameObject(firstB.gameObject);
            }
        }
        
        
    }

    public void ToggleWindow(UI_TYPE type)
    {
        bool enabled = !_ui[type].gameObject.activeInHierarchy;
        SetWindow(type, enabled);
    }

    public void OnGoToMenu()
    {
        //GameFlow.GoToMenu();
        ClearWindows();
    }

    public void OnResume()
    {
        ClearWindows();
    }

    public void OnRestart()
    {
        GameFlow.RestartLevel();
        ClearWindows();

    }

    private void ClearWindows()
    {
        SetWindow(UI_TYPE.TUTORIAL, false);
        SetWindow(UI_TYPE.PAUSE, false);
        SetWindow(UI_TYPE.RESTART, false);
    }

}
