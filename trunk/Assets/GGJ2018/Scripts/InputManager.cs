﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InputManager : MonoBehaviour {

    private EntityController[] players;
    private CameraManager cameraManager;

    void Start () {
        players = FindObjectsOfType<EntityController>().Where((e) => (e.tag == "Player")).ToArray();
    }
	
	void Update () {

        //Debug.LogWarning(Input.GetAxisRaw("Player1Horizontal"));

        if (players.Length <= 0) return;

        players[0].Move(Input.GetAxis("Player1Horizontal"));
        players[1].Move(Input.GetAxis("Player2Horizontal"));

        if (Input.GetButtonDown("Player1Action0"))
        {
           
            players[0].Jump();
            players[1].Jump();
        } 
        if (Input.GetButtonDown("Player1Action1"))
        {
            players[0].ToggleLookDirection();
            players[1].ToggleLookDirection();
        }
        if (Input.GetButtonDown("Player2Action0"))
        {
            players[0].Shoot();
            players[1].Shoot();
        }
        if (Input.GetButtonDown("Player2Action1"))
        {
            players[0].Stomp();
            players[1].Stomp();
        }

        if (Input.GetButtonDown("ShowTutorial"))
        {
            if (cameraManager == null) cameraManager = FindObjectOfType<CameraManager>();
            if (cameraManager != null )
            {
                cameraManager.UI.ToggleWindow(UI_TYPE.TUTORIAL);
            }
        }
        if (Input.GetButtonDown("ShowPause"))
        {
            if (cameraManager == null) cameraManager = FindObjectOfType<CameraManager>();
            if (cameraManager != null)
            {
                cameraManager.UI.ToggleWindow(UI_TYPE.PAUSE);
            }
        }

    }
}
