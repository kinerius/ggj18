﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameFlow{

    public class VictoryCondition {
        public List<VictoryStar> stars;

        public VictoryCondition() {
            stars = new List<VictoryStar>();
        }
    }

    static VictoryCondition condition = new VictoryCondition();

    /*void OnEnable()
    {
        
        SceneManager.sceneLoaded += OnSceneLoaded;
    }*/

    public static void StarCatch() {
        
        bool won = true;
        foreach (var star in condition.stars) {
            won = won && star.catched;
        }
        if (won) {
            WinLevel();
        }
    }

    public static void WinLevel() {
        Scene scene = SceneManager.GetActiveScene();
        condition = new VictoryCondition();
        if (SceneManager.sceneCountInBuildSettings <= scene.buildIndex + 1)
        {
            SceneManager.LoadScene(0);
        }
        else {
            SceneManager.LoadScene(scene.buildIndex + 1);
        }
        
    }

    public static void RestartLevel() {
        Scene scene = SceneManager.GetActiveScene();
        condition = new VictoryCondition();
        SceneManager.LoadScene(scene.buildIndex);
    }

    public static void RegisterStar(VictoryStar newStar) {
        condition.stars.Add(newStar);
    }

    /*void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("LOADED!");
        
        VictoryStar[] stars = GameObject.FindObjectsOfType<VictoryStar>();
        if (stars.Length == 0) {
            WinLevel();
        }
        condition.stars = stars;
        
    }*/


}
