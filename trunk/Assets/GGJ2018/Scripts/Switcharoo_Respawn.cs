﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switcharoo_Respawn : Switcharoo {

    public override void Trigger(EntityController entity)
    {
        if (entity != null)
        {
            if (transform.position.x > entity.spawnPosition.x)
                entity.spawnPosition = transform.position;
        }
    }
}
