﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "GGJ2018/PlayerData", order = 1)]
public class PlayerData : ScriptableObject {

    public Transform view;
    public Transform bullet;

    public float moveSpeed = 1;
    public float jump_height = 3;
    public float bullet_speed = 2;
    public float bullet_rate = 1;
    public float stomp_speed = 3;
    public bool respawnOnDeath = false;
    public float respawnTimer = 2;

    [Header("Effects")]
    public Transform vfx_death;
    public Transform vfx_shoot;
    public Transform vfx_stomp;

    [Header("Sounds")]
    public AudioClip sfx_jump;
    public AudioClip sfx_death;
    public AudioClip sfx_shoot;
    public AudioClip sfx_stompStart;
    public AudioClip sfx_stompEnd;
    public AudioClip sfx_grounded;
    public AudioClip sfx_step;

    [Header("Flags")]
    public bool canBeStomped = false;
    public bool deflectBullets = false;
}
