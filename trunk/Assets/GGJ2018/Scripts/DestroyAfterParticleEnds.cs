﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterParticleEnds : MonoBehaviour {

    ParticleSystem p;

    private void Awake()
    {
        p = GetComponent<ParticleSystem>();
    }

    private void Update()
    {
        if (p.isStopped)
        {
            Destroy(gameObject);
        }
    }
}
