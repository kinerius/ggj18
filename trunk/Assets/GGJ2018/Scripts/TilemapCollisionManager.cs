﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TilemapCollisionManager : MonoBehaviour {

    public Tilemap destroyableTilemap;

    private static TilemapCollisionManager _i;
    public static TilemapCollisionManager i
    {
        get
        {
            return _i;
        }
    }

    private void Awake()
    {
        _i = this;
    }

    public void DestroyTileAt(Vector3 tilePoint)
    {
        if (destroyableTilemap != null)
        {
            destroyableTilemap.SetTile(destroyableTilemap.WorldToCell(tilePoint), null);
        }
    }
}
