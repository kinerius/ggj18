﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "GGJ2018/BulletData", order = 1)]
public class BulletData : ScriptableObject
{
    public float speed = 10;

    public Transform vfx_death;
    public AudioClip sfx_impact;

}
