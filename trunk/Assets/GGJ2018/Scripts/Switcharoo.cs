﻿using UnityEngine;
using System.Collections;

public class Switcharoo : MonoBehaviour
{
    public virtual void Trigger(EntityController entity)
    {
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(false);
        }
    }
}
