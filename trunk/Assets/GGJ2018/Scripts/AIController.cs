﻿using UnityEngine;
using System.Collections;

public class AIController : MonoBehaviour
{
    public enum AI_STATE
    {
        IDLE = 0,
        WALKING = 1,
    }

    EntityController owner;
    AI_STATE state;

    private float actionWait = 0;
    private float lastMoveDirection = 1;

    private LayerMask wallLayerMask;
    private ContactFilter2D filter;
    RaycastHit2D[] raycasts = new RaycastHit2D[2];
    Collider2D myCollider;

    private void Awake()
    {
        owner = GetComponent<EntityController>();

        wallLayerMask = LayerMask.GetMask("DEATH", "Default");
        filter = new ContactFilter2D();
        filter.layerMask = wallLayerMask;
        filter.useLayerMask = true;
    }

    public void Start()
    {
        myCollider =  owner.view.GetComponentInChildren<Collider2D>();
    }

    void Update()
    {
        if (Time.time > actionWait)
        {
            RandomizeAction();
        }

        switch (state)
        {
            case AI_STATE.IDLE:
                owner.Move(0);
                break;
            case AI_STATE.WALKING:

                owner.Move(lastMoveDirection);
                break;
        }

        bool switchDirection = false;

        for (int i = 0; i < raycasts.Length; i++)
        {
            raycasts[i] = new RaycastHit2D();
        }

        if (lastMoveDirection > 0)
        {
            if (Physics2D.Raycast(transform.position, Vector3.right, filter, raycasts, 0.75f) > 0)
            {
                switchDirection = HasCollision(raycasts);
            }
            if (!switchDirection)
            {
                Physics2D.Raycast(transform.position + Vector3.right * 0.75f, Vector3.down, filter, raycasts, 1.25f);
                switchDirection = !HasCollision(raycasts);
            }
        } else if ( lastMoveDirection < 0)
        {
            if (Physics2D.Raycast(transform.position, Vector3.left, filter, raycasts, 1.25f) > 0)
            {
                switchDirection = HasCollision(raycasts);
            }
            if (!switchDirection)
            {
                Physics2D.Raycast(transform.position + Vector3.left * 0.75f, Vector3.down, filter, raycasts, 0.75f);
                switchDirection = !HasCollision(raycasts);
            }
        }

        if (switchDirection)
        {
            lastMoveDirection = lastMoveDirection < 0 ? 1 : -1;
        }
    }

    private bool HasCollision(RaycastHit2D[] hits)
    {
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].collider == null) continue;
            if (hits[i].collider == myCollider) continue;
            Debug.DrawLine(transform.position, hits[i].point, Color.red, 2);
            return true;
        }
        return false;
    }

    private void RandomizeAction()
    {
        float random = Random.Range(0, 100);
        int total = Mathf.RoundToInt(random / 100);
        state = (AI_STATE)total;

        //Debug.Log(name + " rolled " + random + " rounded to " + total + " " + state.ToString()); 
        switch (state)
        {
            case AI_STATE.IDLE:
                actionWait = Time.time + 1;
                break;
            case AI_STATE.WALKING:
                actionWait = Time.time + Random.Range(4,7);
                break;
        }
    }


}
